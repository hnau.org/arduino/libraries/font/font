#ifndef FONT_H
#define FONT_H

#include <Arduino.h>

typedef uint8_t Glyph;

class Font {

public:

	virtual size_t getGlyphWidth() = 0;
	virtual size_t getGlyphHeight() = 0;
	
	virtual bool getPixel(Glyph glyph, size_t x, size_t y) = 0;
	virtual bool checkGlyphIsAvailable(Glyph glyph) = 0;

};


#endif //FONT_H
